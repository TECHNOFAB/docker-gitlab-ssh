FROM alpine:latest

RUN apk add --no-cache git openssh
RUN mkdir -p ~/.ssh && chmod 700 ~/.ssh
RUN ssh-keyscan gitlab.com >> ~/.ssh/known_hosts && chmod 644 ~/.ssh/known_hosts

COPY entrypoint.sh /usr/local/bin/

ENTRYPOINT ["/bin/sh", "/usr/local/bin/entrypoint.sh"]
