# Gitlab SSH Docker Image
This image makes it possible to commit back to the Gitlab repository from within Gitlab CI.

It sets everything up in the entrypoint of the Docker image and pushes any commits you create
in the script section of the job automatically after it's successful execution.

## How to use
```yaml
update-dependencies:
    stage: ...
    image: technofab/docker-gitlab-ssh
    variables:
        GIT_NAME: "hal9000"
        GIT_EMAIL: "hal9000@ci.ci"
        # add GIT_SSH_PRIV_KEY as a secret!
    script:
        - echo "do something here" >> todo.txt
        - git add .
        # [ci skip] is needed to not get an infinite loop of pipelines / alternatively use rules to prevent that
        - git commit -m "automatic update [ci skip]"
```

# How to get the SSH key
You can read about the whole process here, this image is just there to make your CI config a bit shorter:

https://about.gitlab.com/blog/2017/11/02/automating-boring-git-operations-gitlab-ci/
