#!/bin/sh
set -e

echo "Entrypoint of docker-gitlab-ssh. Configuring git..."

cd $CI_PROJECT_DIR

eval "$(ssh-agent -s)" >/dev/null
echo "$GIT_SSH_PRIV_KEY" | tr -d '\r' | ssh-add - >/dev/null
git config --global user.email "$GIT_EMAIL" >/dev/null
git config --global user.name "$GIT_NAME" >/dev/null

export CI_PUSH_REPO=`echo $CI_REPOSITORY_URL | sed -e "s|.*@\(.*\)|git@\1|" -e "s|/|:|"`
git remote rm origin && git remote add origin ${CI_PUSH_REPO}

echo "Preparations finished, running script..."

eval "/bin/sh"

echo "Script finished, pushing if possible..."
git push origin ${GIT_SOURCE_REF:-HEAD}:${GIT_TARGET_REF:-$CI_COMMIT_REF_NAME}
